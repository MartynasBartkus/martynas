﻿using NLog;
using StructureMap;


namespace Servisas
{
    public class LogRegistry : Registry
    {
        public LogRegistry()
        {
            // make any StructureMap configuration here

            // Sets up the default "IFoo is Foo" naming convention
            // for auto-registration within this assembly
            Scan(x =>
            {
                x.WithDefaultConventions();
            });


          //  For<ILogger>().Use<LogHelper>();
        }
    }
}
    