﻿using NLog;
using NLog.Fluent;
using ServiceStack;
using System;
using static Servisas.EntryService;

namespace Servisas
{
    [Route("/status")]
    [Route("/status/{Date}")]
    public class StatusRequest : IReturn<StatusResponse>
    {
        public DateTime Date { get; set; }
    }
    public class StatusResponse
    {
        public int Total { get; set; }
        public int Goal { get; set; }
    }
    public class StatusService : Service
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public object Any(StatusRequest request)
        {           

            var date = request.Date.Date;
            var measureData = (MeasureData)SessionBag[date.ToString()];
            if (measureData == null)
            {
                measureData = new MeasureData { Goal = 500, Total = 400 };
            }
            return new StatusResponse() { Goal = measureData.Goal, Total = measureData.Total };

            //test git
            


        }
        

    }
}