﻿using NLog;
using ServiceStack;
using System;


namespace Servisas
{
    public class Global : System.Web.HttpApplication
    {
        public class CodeTrackerAppHost : AppHostBase
        {
            public CodeTrackerAppHost() : base("Hello Web Services", typeof(EntryService).Assembly) { }

            public static readonly Logger Log = NLog.LogManager.GetLogger(typeof(EntryService).Name);

            public override void Configure(Funq.Container container)
            {

            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            new CodeTrackerAppHost().Init();
        }

        protected void Application_Error()
        {
            Exception lastException = Server.GetLastError();
            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Fatal(lastException);

            logger.Info("Global informational message", 5);
            logger.Warn("Global warning message", 4);
            logger.Error("Global error message", 3);
        }
    }
}