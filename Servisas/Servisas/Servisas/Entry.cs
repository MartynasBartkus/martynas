﻿using NLog;
using ServiceStack;
using StructureMap;
using System;

namespace Servisas
{

    [Route("/entry", "POST")]
    [Route("/entry/{Quantity}/{EntryTime}", "POST")]
    public class Entry : IReturn<EntryResponse>
    {
        public DateTime EntryTime { get; set; }
        public int Quantity { get; set; }
    }

    public class EntryResponse
    {      
        public int Id { get; set; }     
    }

    public class EntryService : Service
    {
      //  private static ILogger logger = LogManager.GetCurrentClassLogger();

        public object Post(Entry request)
        {    
          //  logger.Info("Posting to MyService");

            var date = request.EntryTime.Date;
            var measureData = (MeasureData)SessionBag[date.ToString()];
           
                if (measureData == null)
                {
                    measureData = new MeasureData { Goal = 500 };
                }
                measureData.Total += request.Quantity;
                SessionBag[date.ToString()] = measureData;
 
            string s = null;
            try
            {
                ProcessString(s);
            }
            catch (Exception e)
            {
                //logger.Error("Exception caught."+ e);
                LogHelper logger = new LogHelper();

                //logger.Log(LogLevel.Error, e, "Custom Exception caught");   //Config Nlog.config to accept this type of log 



                logger.Log(LogLevel.Error,e, "Custom Exception caught");
            }

            return new EntryResponse { Id = 1 };



        }

        void ProcessString(string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException();
            }
        }
        public class MeasureData
        {
            public int Total { get; set; }
            public int Goal { get; set; }
        }

    }
}