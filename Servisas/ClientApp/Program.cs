﻿using Servisas;
using ServiceStack;
using System;

namespace ClientApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int quantity = -1;
            try
            {
                var client = new JsonServiceClient("http://localhost:49704");
                do
                {
                    Console.WriteLine("Enter Lines of code (0 to exit)");
                    quantity = int.Parse(Console.ReadLine());

                    var response = client.Send<EntryResponse>(new Entry { Quantity = quantity, EntryTime = DateTime.Now });
                    Console.WriteLine("Response: " + response.Id);
                } while (quantity != 0);


                var postResponse = client.Post<StatusResponse>("Status", new StatusRequest { Date = DateTime.Now });
                Console.WriteLine("{0} of {1} achieved", postResponse.Total, postResponse.Goal);
                Console.ReadLine();
            }
            catch (UriFormatException webEx)
            {
                Console.WriteLine("Wrong URI");
                quantity = int.Parse(Console.ReadLine());
            }
        }
    }
}
